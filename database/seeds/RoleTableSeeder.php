<?php

namespace Database\Seeders;

use App\Role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    private function dataAtual()
    {
        return Carbon::now();
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = Role::all()->count();
        if ($count == 0) {

            echo "Qtde: " . $count . " Povoando...";
            $dataAtual = $this->dataAtual();
            
            Role::create(['name' => 'Manager']); 
            Role::create(['name' => 'Developer']);
            Role::create(['name' => 'Support']);     

            $this->command->info("Done.");
        } else {
            echo "Qtde: " . $count . " Já povoada!";
        }
    }
}
