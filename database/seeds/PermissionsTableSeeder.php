<?php

namespace Database\Seeders;

use App\Permission;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsTableSeeder extends Seeder
{
    private function dataAtual()
    {
        return Carbon::now();
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = Permission::all()->count();
        if ($count == 0) {

            echo "Qtde: " . $count . " Povoando...";
            $dataAtual = $this->dataAtual();
            
            Permission::create(['name' => 'add_post']); 
            Permission::create(['name' => 'edit_post']);
            Permission::create(['name' => 'delete_post']);
            Permission::create(['name' => 'add_user']);
            Permission::create(['name' => 'edit_user']); 
            Permission::create(['name' => 'delete_user']); 
            Permission::create(['name' => 'add_role']); 
            Permission::create(['name' => 'edit_role']);
            Permission::create(['name' => 'delete_role']);
            Permission::create(['name' => 'add_group']); 
            Permission::create(['name' => 'edit_group']);
            Permission::create(['name' => 'delete_group']);            

            $this->command->info("Done.");
        } else {
            echo "Qtde: " . $count . " Já povoada!";
        }
    }
}
