<?php

namespace Database\Seeders;

use App\User;
use App\Role;
use App\Permission;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    private function dataAtual()
    {
        return Carbon::now();
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //truncate tables usando DB
       /* DB::statement("SET foreign_key_checks = 0");
            DB::table('user_role')->truncate();
            DB::table('role_permission')->truncate();
            //Truncate table Utilizando el modelo
            Permission::truncate();
            Role::truncate();
        DB::statement("SET foreign_key_checks = 1");*/
        //User DATA Admin
        $useradmin = User::where('email','admin@admin.com')->first();

        if($useradmin){
            $useradmin->delete();
        }

        $count = User::all()->count();
        if ($count == 0) {

            echo "Qtde: " . $count . " Povoando...";
            $dataAtual = $this->dataAtual();
            
            $useradmin = User::create([
                'name' =>'Admin',
                'email' => 'Admin@admin.com',
                'password' => Hash::make('admin'),
            ]);         

            $this->command->info("Done.");
        } else {
            echo "Qtde: " . $count . " Já povoada!";
        }
    }
}
