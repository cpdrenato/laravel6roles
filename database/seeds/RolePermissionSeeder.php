<?php

namespace Database\Seeders;

use App\Role;
use App\Permission;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolePermissionSeeder extends Seeder
{
    private function dataAtual()
    {
        return Carbon::now();
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        $count = DB::table('role_permission')->count();
        if ($count == 0) {

            echo "Qtde: " . $count . " Povoando...";
            $dataAtual = $this->dataAtual();
            $permissions = Permission::all();

            $admin = Role::whereName('Manager')->first();

            foreach ($permissions as $permission) {
                DB::table('role_permission')->insert(
                    [
                        'role_id' => $admin->id,
                        'permission_id' => $permission->id,
                    ]
                );
            }

            $editor = Role::whereName('Developer')->first();

            foreach ($permissions as $permission) {
                if (!in_array($permission->name, ['edit_roles'])) {
                    DB::table('role_permission')->insert(
                        [
                            'role_id' => $editor->id,
                            'permission_id' => $permission->id,
                        ]
                    );
                }
            }

            $viewer = Role::whereName('Support')->first();

            $viewerRoles = [
                'edit_post',
                'edit_user',
                'edit_role',
                'edit_group',
            ];

            foreach ($permissions as $permission) {
                if (in_array($permission->name, $viewerRoles)) {
                    DB::table('role_permission')->insert(
                        [
                            'role_id' => $viewer->id,
                            'permission_id' => $permission->id,
                        ]
                    );
                }
            }
                     

            $this->command->info("Done.");
        } else {
            echo "Qtde: " . $count . " Já povoada!";
        }
    }
}
