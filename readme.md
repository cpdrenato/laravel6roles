<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## USER ROLES AND PERMISSIONS IN LARAVEL 6

> ROLES E PERMISSÕES DO USUÁRIO NO LARAVEL 6
### Comandos Utilizados:
```
- composer create-project --prefer-dist laravel/laravel laravel6role "6.0.*"

- composer require laravel/ui "^1.0" --dev

- php artisan ui:auth

- php artisan make:migration create_roles_table
- php artisan make:migration create_user_role_table
- php artisan make:migration create_groups_table
- php artisan make:migration create_user_group_table
- php artisan make:migration create_permissions_table
- php artisan make:migration create_role_permission_table
- php artisan migrate
- php artisan db:seed
- php artisan make:model Role
- php artisan make:model Group
- php artisan make:model Permission
- php artisan make:controller UserController
- php artisan make:controller RoleController
- composer dumpautoload
- php artisan serve
```

## Limpar Cache

```
php artisan config:clear
php artisan cache:clear
php artisan view:clear
php artisan route:clear
composer dump-autoload
php artisan optimize:clear
```

- https://blog.renatolucena.net/post/user-roles-and-permissions-in-laravel-6
- https://www.youtube.com/watch?v=KluvIxq2JDU