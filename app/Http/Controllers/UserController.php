<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        $roles = Role::all();
        return View('add_roles_user')->with(array('users'=>$users,'roles'=>$roles));
    }
    public function store(Request $request)
    {
        $role = Role::find($request->role_id);
        $role->users()->detach($request->users);
        $role->users()->attach($request->users);
        return redirect()->route('home');
    }

}
